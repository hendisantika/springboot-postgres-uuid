# Spring Boot Postgres UUID

An example of how to use Spring Boot with Postgres and id that use optional UUIDs for entities. 
If you supply an UUID to the entity, then it will use that UUID to create a new Person in the database. If you do not supply a UUID when creating and saving a new Person, then the persistance layer will generate one for you.

### Things to do :
1. `git clone https://gitlab.com/hendisantika/springboot-postgres-uuid.git`
2. `cd springboot-postgres-uuid`
3. `mvn clean spring-boot:run`