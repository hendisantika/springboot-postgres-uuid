package com.hendisantika.springbootpostgresuuid;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootPostgresUuidApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootPostgresUuidApplication.class, args);
    }

}

