package com.hendisantika.springbootpostgresuuid.controller;

import com.hendisantika.springbootpostgresuuid.model.Person;
import com.hendisantika.springbootpostgresuuid.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-postgres-uuid
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-08
 * Time: 08:10
 */
@RestController("/person")
public class PersonController {

    @Autowired
    PersonRepository personRepository;

    @GetMapping
    public String get(){

        Person person = new Person();
        person.setFirstName("Uzumaki");
        person.setLastName("Naruto");
        personRepository.save(person);

        Person person2 = new Person();
        person2.setLastName("Uchiha");
        person2.setFirstName("Sasuke");
        person2.setId(UUID.randomUUID());
        personRepository.save(person2);

        return "hello";
    }
}