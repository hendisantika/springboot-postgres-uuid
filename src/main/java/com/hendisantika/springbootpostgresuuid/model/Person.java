package com.hendisantika.springbootpostgresuuid.model;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.UUID;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-postgres-uuid
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-08
 * Time: 08:07
 */
@Data
@Entity
public class Person {
    @Id
    @GeneratedValue(generator = "inquisitive-uuid")
    @GenericGenerator(name = "inquisitive-uuid", strategy = "com.hendisantika.springbootpostgresuuid.util.InquisitiveUUIDGenerator")
    private UUID id;
    private String firstName;
    private String lastName;

}
