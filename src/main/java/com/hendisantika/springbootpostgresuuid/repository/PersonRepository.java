package com.hendisantika.springbootpostgresuuid.repository;

import com.hendisantika.springbootpostgresuuid.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-postgres-uuid
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-08
 * Time: 08:07
 */
public interface PersonRepository extends JpaRepository<Person, UUID> {
}
